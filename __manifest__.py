{
    'name': 'Odoo update cron',
    'version': '14.0.1.0.0',
    'category': 'sale',
    'summary': """Odoo update orden cron""",
    'description': """
        Crea el cron para que se puedan actualizar los pedidos de un dia anterior
    """,
    'author': 'Prixz',
    'depends': ['woo_multi_warehouse'],
    'data': [
        'data/ir_cron.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
