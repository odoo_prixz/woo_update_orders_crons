# -*- coding: utf-8 -*-
from odoo import api, fields, models
from datetime import datetime, timedelta

import logging
_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def filter_order_using_date(self, woocommerce, date, page, page_size):
        orders = woocommerce.get(
            'orders',
            params={
                'after': date,
                'page': page,
                'per_page': page_size,
                'order': 'asc'
            }
        ).json()
        return orders

    def update_order_status(self, orders):
        for order in orders:
            order_feed = self.env["order.feed"].search([
                ("name", "=", order["id"])
            ])
            order_id = self.env["sale.order"].search([
                ("name", "=", order["id"])
            ])
            if order_feed and order_id and order_feed.order_state != order["status"]:
                order_feed.order_state = order["status"]
                if order["date_paid"]:
                    order_feed.woo_date_paid = (datetime.fromisoformat(order['date_paid']) + timedelta(hours=6))
                    order_id.woo_date_paid = order_feed.woo_date_paid

    def _cron_update_status_order(self):
        channel_id = self.env['multi.channel.sale'].sudo().search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate")
        ], limit=1)
        if channel_id:
            woocommerce = channel_id._get_woocommerce_connection()
            if woocommerce:
                contador = 1
                while True:
                    orders = self.filter_order_using_date(woocommerce, datetime.now() - timedelta(days=1), contador, 50)
                    contador += 1
                    if not orders:
                        break
                    self.update_order_status(orders)
                    print("hola")
